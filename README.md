# Инструкция по установке сервера конфигуратора

## Установка необходимого ПО

### Ubuntu 

``` apt-get install git nano unzip```

### CentOS

``` yum install git nano unzip ```

### Установка Docker и Docker-compose

- [Docker](https://docs.docker.com/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

### Клонирование репозитория 

* Переходим в любую папку (например в /opt)

  `cd /opt`

* Клонируем репозиторий

  `git clone https://bitbucket.org/anterra_serv/ant_updserv.git`

* Переходим в папку

  `cd ant_conf_serv`

* Запускаем скрипт разворачивания

  `./setup.sh` - откроется текстовый редактор для редактирования основных параметров

* Редактирование параметров:
  * Убрать первые # (раскоментировать)
  * Написать подходящие параметры
    * TZ - таймзона (например TZ=Etc/UTC)
    * LETSENCRYPT=true (использовать LETSENCRYPT оставить в true)
    * LE_EMAIL - email ящик для летсенкрипт (например LE_EMAIL=test@example.com)
    * LE_FQDN - домен сервера, должа быть A запись на ip (например LE_FQDN=example.com)
    * BRAND_ID - Ваш бренд, уточнить у менеджера (например BRAND_ID=_ALL_NEWBUILD) 
    * APPMODE - мод (режим) приложения, уточнить у менеджера (например APPMODE=STB)
    * PRODUCT -  версия продукта, уточнить у менеджера (например USER_AGENT=Home.TVPlayer)

```yaml
version: '2'
services:
        nginx:
                image: mntanterra/nginx:v3
                restart: always
                container_name: nginx
                ports:
                        - "80:80"
                        - "443:443"
                volumes:

                        - ./serv.anterra.app:/srv/serv.anterra.app
                        - ./logs:/var/log/nginx
                        - ./report:/srv/report
                        - ./nginx/geo:/etc/nginx/geo
                environment:
                        # - TZ=Etc/UTC # <- uncomment and add your timezone
                        # - LETSENCRYPT=true # <- uncomment if you use ssl
                        # - LE_EMAIL=test@example.com # <- uncomment and add your email
                        # - LE_FQDN=example.com # <- uncomment and add your FQDN name
                logging:
                        driver: json-file
                        options:
                            max-size: "10m"
                            max-file: "5"

        filebrowser:
                image: mntanterra/filebrowser:v3
                restart: always
                container_name: filebrowser

                volumes:
                        - ./filebrowser/brand:/brand
                        - ./filebrowser/database:/database
                        - ./serv.anterra.app:/srv
                environment:
                        - BRAND_NAME=ANTERRA UPDSERV
                logging:
                        driver: json-file
                        options:
                            max-size: "10m"
                            max-file: "5"
        update:
               image: mntanterra/update:v3
               restart: always
               container_name: update
               volumes:
                       - ./serv.anterra.app:/srv
               environment:
                       # - BRAND_ID=_ALL_NEWBUILD # <- set your brand
                       # - APPMODE=STB # <- set your application mode
                       # - PRODUCT=Home.TVPlayer,Home.TVPlayerPlus,TVPlayer,TVPlayerPlus # <- set your version
               logging:
                        driver: json-file
                        options:
                            max-size: "10m"
                            max-file: "5"
        goaccess:
                image: mntanterra/goaccess:v1
                restart: always
                container_name: goaccess
                volumes:
                        - ./logs:/srv/logs
                        - ./report/logstat:/srv/report
                logging:
                        driver: json-file
                        options:
                            max-size: "10m"
                            max-file: "5"
```

  



* Сохранить и закрыть файл

  `Ctrl+O`

  `Ctrl+X`

* Теперь подождать, докер композ соберет все контейнеры и запустит сервис